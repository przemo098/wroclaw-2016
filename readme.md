# Course details

Course on advanced programming techniques.
Topics covered: 

   * Web Technologies: Angular 2 + NodeJS tools
   * Blockchain: technical introduction


# Lectures

   * [Lecture 1](https://docs.google.com/presentation/d/1Wz_szSyHGyPrDQfMca9_ntRRHkC21jLbiOddQ9Akg28/edit?usp=sharing)
   * [Lecture 2](https://docs.google.com/presentation/d/1zxme1rtnNt5P0rVYyhXKJMVKt6kP7fTy2oBeRRPD89E/edit?usp=sharing)


# Labs

## Lab 00

   1. Install NodeJS on your laptop/computer. Version 5.10.1
   2. Install 'gulp' globally on your system, by running: *npm install -g gulp*        
   3. Go and check the Hello World tutorial on [https://angular.io](https://angular.io/)  
   4. Try to replicate the code, and get it to run on your laptop.

## Lab 01

   1. Go to lab01 in this repo
   2. Run

```
#!bash

npm install
gulp
gulp serve
```

Notes: it is OK to accept the two WARNINGS about version mismatch - the newer versions have certain things fixed, better to use them. 


# Homeworks

## Task 1

In this folder

        QmZMTpjjDBc3RzQsNR4tnh6Wu7iTG5BPB9N1PAnL27VLcp

create a file: *yourname.surname-email.txt*
and put your email inside. Add your newly created file to IPFS. Remember the new hash somewhere.


## Task 2

   1. Clone the fork of this repo.
   2. Make a pull-request to upstream, with your name added to students.txt file.

## Task 3

   1. Modify Lab01 so that it uses Twitter Bootstrap, or Google's Material Design UI elements.
   2. Make it look *nicer*.
   3. Make a pull request with your changes.